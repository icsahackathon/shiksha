package finiteloop.com.shiksha;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SignInActivity extends AppCompatActivity {

    TextView mSignUpTextView;
    EditText mEmailEditText, mPasswordEditText;
    Button mSignInButton;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        mEmailEditText = (EditText) findViewById(R.id.activity_sign_in_emailText);
        mPasswordEditText = (EditText) findViewById(R.id.activity_sign_in_passwordText);
        mSignInButton = (Button) findViewById(R.id.signInButton);
        mSignUpTextView = (TextView) findViewById(R.id.sign_up_redirect_text_view);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Log.d("firebase auth", "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    Log.d("firebase auth", "onAuthStateChanged:signed_out");
                }
            }
        };

        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signInWithEmailAndPassword(mEmailEditText.getText().toString(), mPasswordEditText.getText().toString())
                        .addOnCompleteListener(SignInActivity.this,
                                new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (!task.isSuccessful())
                                            Toast.makeText(getApplicationContext(), "Check your Username or Password! " + task.getException(), Toast.LENGTH_LONG).show();
                                        else {
                                            Toast.makeText(getApplicationContext(), "Welcome! You're Signed In Successfully", Toast.LENGTH_SHORT).show();
                                            startActivity(new Intent(SignInActivity.this, VideoActivity.class));
                                        }
                                    }
                                });
            }
        });

        mSignUpTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignInActivity.this, SignUpActivity.class));
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

}
