package finiteloop.com.shiksha;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class DoubtActivity extends AppCompatActivity {

    RecyclerView mDoubtRecyclerView;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doubt);

        toolbar = (Toolbar) findViewById(R.id.activity_doubt_toolbar);
        mDoubtRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mDoubtRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        mDoubtRecyclerView.setAdapter(new DoubtRecyclerViewAdapter());

        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_action_back_wite));
        }

    }

    class DoubtRecyclerViewHolder extends RecyclerView.ViewHolder {

        public DoubtRecyclerViewHolder(View itemView) {
            super(itemView);
        }
    }

    class DoubtRecyclerViewAdapter extends RecyclerView.Adapter<DoubtRecyclerViewHolder> {

        @Override
        public DoubtRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.doubt_card_item, parent, false);
            return new DoubtRecyclerViewHolder(view);
        }

        @Override
        public void onBindViewHolder(DoubtRecyclerViewHolder holder, int position) {

        }

        @Override
        public int getItemCount() {
            return 5;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
