package finiteloop.com.shiksha;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;

public class SignUpActivity extends AppCompatActivity{

    Spinner mTypeSpinner, mSubjectSpinner;
    EditText mEmail, mPassword;
    Button signUpButton;
    LinearLayout mSubjectsLinearLayout;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Log.d("firebase auth", "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    Log.d("firebase auth", "onAuthStateChanged:signed_out");
                }
            }
        };


        signUpButton = (Button) findViewById(R.id.signUpButton);
        mEmail = (EditText) findViewById(R.id.emailText);
        mPassword = (EditText) findViewById(R.id.passwordText);
        mTypeSpinner = (Spinner) findViewById(R.id.type_spinner);
        mSubjectSpinner = (Spinner) findViewById(R.id.subjects_spinner);
        mSubjectsLinearLayout = (LinearLayout) findViewById(R.id.subjects_linear_layout);

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.createUserWithEmailAndPassword(mEmail.getText().toString(), mPassword.getText().toString())
                        .addOnCompleteListener(finiteloop.com.shiksha.SignUpActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(SignUpActivity.this, "You're Successfully Signed Up!", Toast.LENGTH_SHORT).show();
                                }
                                if (!task.isSuccessful()) {
                                    Toast.makeText(SignUpActivity.this, "There was a problem in signing you up, try again later! " + task.getException(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });

        ArrayList<String> mType = new ArrayList<>();
        mType.add("Student");
        mType.add("Teacher");
        mType.add("Volunteer");

        ArrayList<String> subjects = new ArrayList<>();
        subjects.add("Chemistry");
        subjects.add("Physics");
        subjects.add("Maths");

        ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mType);
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mTypeSpinner.setAdapter(typeAdapter);

        ArrayAdapter<String> subjectAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, subjects);
        subjectAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSubjectSpinner.setAdapter(subjectAdapter);

        mTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                if (item.equalsIgnoreCase("Teacher")) {
                    mSubjectsLinearLayout.setVisibility(View.VISIBLE);
                } else {
                    mSubjectsLinearLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
}
